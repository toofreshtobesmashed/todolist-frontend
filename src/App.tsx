import { useState, useEffect } from "react";
import TodoComponent from "./components/TodoComponent";
import { TodoType, TodoEditDtoType } from "./components/Types";
import "./App.css";
import config, { BASE_URL } from "./config";

function App() {
  const [todoList, setTodoList] = useState<TodoType[]>([]);
  const [newText, setNewText] = useState("");
  const [currentEditedTask, setCurrentEditedTask] = useState(0);

  async function addTodo() {
    const text = newText.trim();
    if (!text) return;
    const newTodo: TodoType = {
      text: text,
      isEditing: false,
      isDone: false,
      id: 0,
      deleteTask,
      toggleTaskCompleted,
      editTask,
      enterEditMode,
      exitEditMode,
    };

    const response = await fetch(`${BASE_URL}/todos`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(newTodo),
    });
    const data = await response.json();

    setTodoList([...todoList, data]);
    setNewText("");
  }

  function enterEditMode(id: number) {
    setCurrentEditedTask(id);
  }

  function exitEditMode() {
    setCurrentEditedTask(0);
  }

  async function deleteTask(id: number) {
    try {
      await fetch(`${BASE_URL}/todos/${id}`, { method: "DELETE" });
      setTodoList(todoList.filter((todo) => todo.id !== id));
      console.log("Task deleted successfully:", id);
    } catch (error) {
      console.error("Error deleting task:", error);
    }
  }

  async function fetchTodosFromBackend() {
    try {
      const response = await fetch(`${BASE_URL}/todos`, { method: "GET" });
      if (!response.ok) {
        throw new Error("Failed to fetch todos");
      }
      const data = await response.json();
      setTodoList(data);
    } catch (error) {
      console.error("Error fetching todos:", error);
    }
  }

  async function editTask(id: number, newText: string) {
    const text = newText.trim();
    if (!text) return;
    const editedTodo: TodoEditDtoType = { text: text };

    await fetch(`${BASE_URL}/todos/${id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(editedTodo),
    });

    setTodoList(
      todoList.map((todo) => {
        if (id === todo.id) {
          return { ...todo, text: newText };
        }
        return todo;
      })
    );
  }

  async function toggleTaskCompleted(id: number) {
    const foundTodo = todoList.find((todo) => todo.id == id);
    
    if (!foundTodo) return;
    const newIsDone = !foundTodo.isDone;

    await fetch(`${BASE_URL}/todos/${id}/done/${newIsDone}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
    });

    setTodoList(
      todoList.map((todo) => {
        if (id === todo.id) {
          return { ...todo, isDone: newIsDone };
        }
        return todo;
      })
    );
  }

  function onChangeText(text: string) {
    if (text.length > config.maximumTodoTextLength) return;
    setNewText(text);
  }

  useEffect(() => {
    fetchTodosFromBackend();
  }, []);

  return (
    <div className="container">
      <div className="main-div">
        <h1>TodoList</h1>
        <div className="todolist">
          {todoList.map((todo) => (
            <TodoComponent
              text={todo.text}
              isDone={todo.isDone}
              id={todo.id}
              isEditing={todo.id === currentEditedTask}
              key={todo.id}
              deleteTask={() => deleteTask(todo.id)}
              editTask={editTask}
              toggleTaskCompleted={() => toggleTaskCompleted(todo.id)}
              enterEditMode={enterEditMode}
              exitEditMode={exitEditMode}
            />
          ))}
        </div>
        <div>
          <input
            type="text"
            placeholder="napis text"
            value={newText}
            onChange={(e) => onChangeText(e.target.value)}
          />
          <button onClick={addTodo}>Potvrd</button>
        </div>
        <button
          onClick={() => {
            setTodoList([]);
          }}
        >
          Vymaz vsetky
        </button>
      </div>
    </div>
  );
}

export default App;
