import {
  BsTrash3,
  BsPencilFill,
  BsCheckLg,
  BsFillXOctagonFill,
} from "react-icons/bs";
import { useState } from "react";
import config from "../config";
import { nanoid } from "nanoid";
import {TodoType} from "./Types"


function TodoComponent(props: TodoType) {
  let [editedText, setEditedText] = useState("");
  let [invalid, setInvalid] = useState(false);
  let [inputRerenderKey, setInputRerenderKey] = useState("");

  function confirmEditTask() {
    if (editedText.length === 0) {
      setInvalid(true);
      setInputRerenderKey(nanoid())
      return;
    }
    props.exitEditMode();
    props.editTask(props.id, editedText);
  }

  function cancelEditTask() {
    props.exitEditMode();
  }

  function onChangeEditedText(newText: string) {
    if (newText.length > config.maximumTodoTextLength) return;
    setEditedText(newText);
  }

  function enterEditMode() {
    setEditedText(props.text);
    props.enterEditMode(props.id);
  }

  const normalTemplate = (
    <>
      <input
        type="checkbox"
        checked={props.isDone}
        onChange={() => {
          props.toggleTaskCompleted(props.id);
        }}
      />
      <span className="todolist-item-text">{props.text}</span>
      <BsTrash3 className="icon" onClick={() => props.deleteTask(props.id)} />
      <BsPencilFill className="icon" onClick={enterEditMode} />
    </>
  );

  const editingTemplate = (
    <>
      <input
        type="text"
        value={editedText}
        onChange={(e) => onChangeEditedText(e.target.value)}
        className={invalid ? "invalid" : ""}
        key={inputRerenderKey}
      />
      <BsFillXOctagonFill className="icon" onClick={cancelEditTask} />
      <BsCheckLg className="icon" onClick={confirmEditTask} />
    </>
  );
  return (
    <div className="todolist-item">
      {props.isEditing ? editingTemplate : normalTemplate}
    </div>
  );
}

export default TodoComponent;
