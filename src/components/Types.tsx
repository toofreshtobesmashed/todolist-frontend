export type TodoType = {
  id: number;
  text: string;
  isDone: boolean;
  isEditing: boolean;
  deleteTask: (id: number) => void;
  toggleTaskCompleted: (id: number) => void;
  editTask: (id: number, newText: string) => void;
  enterEditMode: (id: number) => void;
  exitEditMode: () => void;
};

export type TodoEditDtoType = {
  text: string;
};
