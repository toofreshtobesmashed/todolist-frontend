export const config = {
  maximumTodoTextLength: 22,
} as const;

export const BASE_URL = "http://localhost:8080";

export default config;
